#!/usr/bin/env python

import h5py
import numpy as np
from matplotlib import pyplot as plt

f1 = h5py.File("fitted-parameters-6ampl-3phase.hdf5", "r")
f2 = h5py.File("simulation3-6ampl-3phase.hdf5", "r")
f2 = h5py.File("simulation3-no-init-6ampl-3phase.hdf5", "r")
f2 = h5py.File("parameters-no-init-6ampl-3phase.hdf5", "r")

p1 = f1["parameters"][:]
p2 = f2["parameters"][:]


nr_timeslots          = f2["parameters"].attrs["nr_timeslots"]
nr_timesteps_per_slot = f2["parameters"].attrs["nr_timesteps_per_slot"]
nr_parameters_ampl    = f2["parameters"].attrs["nr_parameters_ampl"]
nr_parameters_phase   = f2["parameters"].attrs["nr_parameters_phase"]
time_start            = f2["parameters"].attrs["time_start"]
time_step             = f2["parameters"].attrs["time_step"]
subgrid_size          = f2["parameters"].attrs["subgrid_size"]
image_size            = f2["parameters"].attrs["image_size"]


p1 = p1[:22,:,6:].reshape((-1,62,40,3)).transpose((0,2,1,3)).reshape(-1,62,3)
p2 = p2[:22,:,6:].reshape((-1,62,40,3)).transpose((0,2,1,3)).reshape(-1,62,3)

timerange = np.arange(p1.shape[0]) * nr_timesteps_per_slot * time_step / 3600.0

#for i in find(sqrt((abs(p1-p2)**2).mean(axis=0).mean(axis=1))>2):
#for i in [61]: #[55,56]:
for i in [55,56,60,61]:
    plt.figure()
    plt.title("Station nr. {0}".format(i))
    #plt.plot(timerange, p1[:,i,:] - p1[:,0,:])
    plt.gca().set_color_cycle(None)
    plt.plot(timerange[:p2.shape[0]], p2[:,i,:] - p2[:,0,:],'.-')
    plt.legend(("constant", "slope l-direction", "slope m-direction", "estimated constant", "estimated slope l-direction", "estimated slope m-direction"))
    plt.xlabel("time (hours)")
    plt.ylabel("phase coefficient")
#    plt.savefig("simulation-station-{0}.eps".format(i))

plt.show()
