import numpy as np

def next_composite(n):
    n += (n & 1)
    while True:
        nn = n
        while ((nn % 2) == 0): nn /= 2
        while ((nn % 3) == 0): nn /= 3
        while ((nn % 5) == 0): nn /= 5
        if (nn == 1): return n
        n += 2

def mask_aterm(aterm, support):
    subgrid_size = aterm.shape[-2]
    m = abs((np.arange(subgrid_size) +subgrid_size/2) % subgrid_size - subgrid_size/2) < ((support+1) //2)
    A = np.fft.fft2(aterm, axes=(-3,-2)) * m[:,np.newaxis,np.newaxis] * m[np.newaxis,:,np.newaxis]
    return np.fft.ifft2(A, axes=(-3,-2))
