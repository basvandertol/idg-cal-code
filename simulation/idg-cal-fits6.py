#!/usr/bin/env python


import numpy as np
import h5py
import scipy.constants as sc
import pyrap.tables
import signal
import argparse
import time
import idg

import matplotlib as mpl
mpl.use('Agg')
import idg.util as util

import astropy.io.fits as fits
import scipy.linalg
from idgwindow import idgwindow
from utils import *


ms = "/var/scratch/bvdtol/idg-demo/data/simulation.ms"
ms = "/var/scratch/bvdtol/P23Hetdex20/L232875_SB120_uv.dppp.pre-cal_124B2FCD4t_144MHz.pre-cal.ms.archive"

imagename = "/var/scratch/bvdtol/P23Hetdex20/image_full_ampphase_di_m.NS.int.model.fits"


print
print ms, imagename

h = fits.getheader(imagename)

padding = 1.5

N00 = h["NAXIS1"]
N0 = 9600
N = next_composite(int(N0 * padding))

cell_size = abs(h["CDELT1"]) / 180 * np.pi
image_size = N * cell_size
print N0, N, image_size

datacolumn = "DATA"

######################################################################
# Open measurementset
######################################################################
table = pyrap.tables.taql("SELECT * FROM $ms WHERE ANTENNA1 != ANTENNA2")

# Read parameters from measurementset
t_ant = pyrap.tables.table(table.getkeyword("ANTENNA"))
t_spw = pyrap.tables.table(table.getkeyword("SPECTRAL_WINDOW"))
frequencies = np.asarray(t_spw[0]['CHAN_FREQ'], dtype=np.float32)


######################################################################
# Parameters
######################################################################
nr_stations      = len(t_ant)
nr_baselines     = (nr_stations * (nr_stations - 1)) / 2
nr_channels      = table[0][datacolumn].shape[0]

nr_timeslots     = 40
nr_timesteps_per_slot = 4

nr_timesteps     =  nr_timeslots * nr_timesteps_per_slot

nr_correlations  = 4
grid_size        = N
subgrid_size     = 32

taper_support    = 7
wterm_support    = 5
aterm_support    = 5

kernel_size      = taper_support + wterm_support + aterm_support

nr_parameters_ampl = 6
nr_parameters_phase = 3
nr_parameters0 = nr_parameters_ampl + nr_parameters_phase
nr_parameters = nr_parameters_ampl + nr_parameters_phase*nr_timeslots

solver_update_gain = 0.5
pinv_tol = 1e-9

init_sol = False

#########################################

input_filename = "fitted-parameters-{0}ampl-{1}phase.hdf5".format(nr_parameters_ampl, nr_parameters_phase)
with h5py.File(input_filename, "r") as input_file:
    input_dataset = input_file["parameters"]
    parameters0 = input_dataset[:]


#output_filename = "simulation3-no-init-{0}ampl-{1}phase.hdf5".format(nr_parameters_ampl, nr_parameters_phase)
output_filename = "parameters-no-init-{0}ampl-{1}phase.hdf5".format(nr_parameters_ampl, nr_parameters_phase)
try:
    output_file = h5py.File(output_filename, "w")
    output_dataset = output_file.create_dataset("parameters", shape=(0,nr_stations, nr_parameters), maxshape=(None, nr_stations, nr_parameters), dtype=np.float32, chunks = True)
except:
    pass


output_dataset.attrs["nr_timeslots"] = nr_timeslots
output_dataset.attrs["nr_timesteps_per_slot"] = nr_timesteps_per_slot
output_dataset.attrs["nr_parameters_ampl"] = nr_parameters_ampl
output_dataset.attrs["nr_parameters_phase"] = nr_parameters_phase
output_dataset.attrs["time_step"] = table[0]['INTERVAL']
output_dataset.attrs["time_start"] = table[0]['TIME']
output_dataset.attrs["subgrid_size"] = subgrid_size
output_dataset.attrs["image_size"] = image_size
output_dataset.attrs["imagename"] = imagename

output_dataset.attrs["solver_update_gain"] = solver_update_gain
output_dataset.attrs["pinv_tol"] = pinv_tol

output_file.flush()

# Initialize empty buffers
rowid        = np.zeros(shape=(nr_baselines, nr_timesteps),
                        dtype=np.int)
uvw          = np.zeros(shape=(nr_baselines, nr_timesteps),
                        dtype=idg.uvwtype)
visibilities = np.zeros(shape=(nr_baselines, nr_timesteps, nr_channels,
                            nr_correlations),
                        dtype=idg.visibilitiestype)
weights      = np.zeros(shape=(nr_baselines, nr_timesteps, nr_channels,
                            nr_correlations),
                        dtype=np.float32)
baselines    = np.zeros(shape=(nr_baselines),
                        dtype=idg.baselinetype)
img          = np.zeros(shape=(nr_correlations, grid_size, grid_size),
                        dtype=idg.gridtype)


#proxy = idg.CPU.Optimized(nr_correlations, subgrid_size)

proxy_ref = idg.CPU.Optimized(nr_correlations, subgrid_size)


if "proxy" not in globals():
    proxy = idg.HybridCUDA.GenericOptimized(nr_correlations, subgrid_size)

if "taper" not in globals():
    # Initialize taper
    taper = idgwindow(subgrid_size, taper_support, padding)
    taper2 = np.outer(taper, taper).astype(np.float32)

    taper_ = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(taper)))
    taper_grid = np.zeros(grid_size, dtype=np.complex128)
    taper_grid[(grid_size-subgrid_size)/2:(grid_size+subgrid_size)/2] = taper_ * np.exp(-1j*np.linspace(-np.pi/2, np.pi/2, subgrid_size, endpoint=False))
    taper_grid = np.fft.fftshift(np.fft.ifft(np.fft.ifftshift(taper_grid))).real*grid_size/subgrid_size

    d = fits.getdata(imagename)
    d = d[:,:, N00/2-N0/2:N00/2+N0/2,N00/2-N0/2:N00/2+N0/2]

    print "{0} non-zero pixels.".format(np.sum(d != 0.0))

    taper_grid0 = taper_grid[(N-N0)/2:(N+N0)/2]

    img[0, (N-N0)/2:(N+N0)/2, (N-N0)/2:(N+N0)/2] = d[0,0,:,:]/np.outer(taper_grid0, taper_grid0)
    img[3, (N-N0)/2:(N+N0)/2, (N-N0)/2:(N+N0)/2] = d[0,0,:,:]/np.outer(taper_grid0, taper_grid0)

    #grid = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(img, axes = (1,2))), axes=(1,2)).astype(np.complex64)

    grid = img.copy()
    proxy.transform(idg.ImageDomainToFourierDomain, grid)

p = []

nr_timesteps_in_ms = len(pyrap.tables.taql("SELECT UNIQUE TIME FROM $table"))


for interval_idx, interval_start in enumerate(range(0,nr_timesteps_in_ms-nr_timesteps+1, nr_timesteps)):

    start_row = nr_baselines * interval_start
    nr_rows = nr_baselines * nr_timesteps

    # Read nr_timesteps samples for all baselines including auto correlations
    timestamp_block = table.getcol('TIME',
                                    startrow = start_row,
                                    nrow = nr_rows)
    antenna1_block  = table.getcol('ANTENNA1',
                                    startrow = start_row,
                                    nrow = nr_rows)
    antenna2_block  = table.getcol('ANTENNA2',
                                    startrow = start_row,
                                    nrow = nr_rows)
    uvw_block       = table.getcol('UVW',
                                    startrow = start_row,
                                    nrow = nr_rows)
    vis_block       = table.getcol(datacolumn,
                                    startrow = start_row,
                                    nrow = nr_rows)
    weight_block    = table.getcol("WEIGHT_SPECTRUM",
                                    startrow = start_row,
                                    nrow = nr_rows)
    flags_block     = table.getcol('FLAG',
                                    startrow = start_row,
                                    nrow = nr_rows)
    weight_block = weight_block * ~flags_block
    vis_block[np.isnan(vis_block)] = 0


    rowid_block     = np.arange(nr_rows)

    uvw_block[:,1:3] = -uvw_block[:,1:3]

    print start_row, nr_rows

    # Change precision
    uvw_block = uvw_block.astype(np.float32)

    # Reshape data
    antenna1_block = np.reshape(antenna1_block,
                                newshape=(nr_timesteps, nr_baselines))
    antenna2_block = np.reshape(antenna2_block,
                                newshape=(nr_timesteps, nr_baselines))
    uvw_block = np.reshape(uvw_block,
                            newshape=(nr_timesteps, nr_baselines, 3))
    vis_block = np.reshape(vis_block,
                            newshape=(nr_timesteps, nr_baselines,
                                        nr_channels, nr_correlations))
    weight_block = np.reshape(weight_block,
                            newshape=(nr_timesteps, nr_baselines,
                                        nr_channels, nr_correlations))
    rowid_block = np.reshape(rowid_block,
                                newshape=(nr_timesteps, nr_baselines))

    # Transpose data
    for t in range(nr_timesteps):
        for bl in range(nr_baselines):
            # Set baselines
            antenna1 = antenna1_block[t][bl]
            antenna2 = antenna2_block[t][bl]

            baselines[bl] = (antenna1, antenna2)

            # Set uvw
            uvw_ = uvw_block[t][bl]
            uvw[bl][t] = uvw_

            # Set visibilities
            visibilities[bl][t] = vis_block[t][bl]

            weights[bl][t] = weight_block[t][bl] #* (antenna1 != 51) * (antenna2 != 51)

            rowid[bl][t] = rowid_block[t][bl]

    # Grid visibilities
    w_step = 400.0

    shift = np.array((0.0, 0.0, 0.0), dtype=np.float32)

    aterms         = util.get_identity_aterms(
                        nr_timeslots, nr_stations, subgrid_size, nr_correlations)
    aterms_offsets = util.get_example_aterms_offset(
                        nr_timeslots, nr_timesteps)

    B0 = np.ones((1, subgrid_size, subgrid_size,1))

    s = image_size/subgrid_size*(subgrid_size-1)
    l = s * np.linspace(-0.5, 0.5, subgrid_size)
    m = -s * np.linspace(-0.5, 0.5, subgrid_size)

    B1,B2 = np.meshgrid(l,m)

    B1 = B1[np.newaxis, :, :, np.newaxis]
    B2 = B2[np.newaxis, :, :, np.newaxis]
    B3 = B1*B1
    B4 = B2*B2
    B5 = B1*B2
    B6 = B1*B1*B1
    B7 = B1*B1*B2
    B8 = B1*B2*B2
    B9 = B2*B2*B2
    B10 = B1*B1*B1*B1
    B11= B1*B1*B1*B2
    B12 = B1*B1*B2*B2
    B13 = B1*B2*B2*B2
    B14 = B2*B2*B2*B2
    B15 = B1*B1*B1*B1*B1
    B16 = B1*B1*B1*B1*B2
    B17 = B1*B1*B1*B2*B2
    B18 = B1*B1*B2*B2*B2
    B19 = B1*B2*B2*B2*B2
    B20 = B2*B2*B2*B2*B2

    base_polynomial = np.concatenate((B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,B16,B17,B18,B19,B20))

    base_polynomial_ampl = base_polynomial[:nr_parameters_ampl].reshape((-1, subgrid_size*subgrid_size)).T
    U,S,V, = np.linalg.svd(base_polynomial_ampl)
    base_orthonormal_ampl = U[:, :nr_parameters_ampl]
    Tampl = np.dot(np.linalg.pinv(base_polynomial_ampl), base_orthonormal_ampl)
    base_orthonormal_ampl = base_orthonormal_ampl.T.reshape((-1, subgrid_size,subgrid_size, 1))

    base_polynomial_phase = base_polynomial[:nr_parameters_phase].reshape((-1, subgrid_size*subgrid_size)).T
    U,S,V, = np.linalg.svd(base_polynomial_phase)
    base_orthonormal_phase = U[:, :nr_parameters_phase]
    Tphase = np.dot(np.linalg.pinv(base_polynomial_phase), base_orthonormal_phase)
    base_orthonormal_phase = base_orthonormal_phase.T.reshape((-1, subgrid_size,subgrid_size, 1))

    Bampl = np.kron(base_orthonormal_ampl, np.array([1.0, 0.0, 0.0, 1.0]))
    Bphase = np.kron(base_orthonormal_phase, np.array([1.0, 0.0, 0.0, 1.0]))

    print "calibrate_init..."
    proxy.calibrate_init(
        w_step,
        shift,
        cell_size,
        kernel_size,
        subgrid_size,
        frequencies,
        visibilities,
        weights,
        uvw,
        baselines,
        grid,
        aterms_offsets,
        taper2)
    print "done."

    X0 =  np.ones((nr_stations, 1))
    X1 =  np.zeros((nr_stations, 1))

    # initialize parameters
    parameters = np.concatenate((X0,) + (nr_parameters-1)*(X1,), axis=1)
    aa = Bampl.sum(axis=1).sum(axis=1)
    parameters[:,:] = np.concatenate((aa[:nr_parameters_ampl,0], np.zeros((nr_parameters_phase*nr_timeslots,))))[np.newaxis, :]

    if init_sol:
        parameters = parameters0[interval_idx].copy()

    for i in range(nr_stations):
        parameters[i,:nr_parameters_ampl] = np.dot(np.linalg.inv(Tampl), parameters[i,:nr_parameters_ampl])
        for j in range(nr_timeslots):
            parameters[i,nr_parameters_ampl+j*nr_parameters_phase:nr_parameters_ampl+(j+1)*nr_parameters_phase] = \
                np.dot(np.linalg.inv(Tphase), parameters[i,nr_parameters_ampl+j*nr_parameters_phase:nr_parameters_ampl+(j+1)*nr_parameters_phase])


    aterm_ampl = np.tensordot(parameters[:,:nr_parameters_ampl] , Bampl, axes = ((1,), (0,)))
    aterm_phase = np.exp(1j*np.tensordot(parameters[:,nr_parameters_ampl:].reshape((nr_stations, nr_timeslots, nr_parameters_phase)), Bphase, axes = ((2,), (0,))))
    #aterms[:,:,:,:,:] = mask_aterm(aterm_phase.transpose((1,0,2,3,4))*aterm_ampl, aterm_support)
    aterms[:,:,:,:,:] = aterm_phase.transpose((1,0,2,3,4))*aterm_ampl

    nr_iterations = 0
    converged = False

    max_dx = 0.0

    timer = -time.time()

    timer0 = 0
    timer1 = 0

    previous_residual = 0.0

    while True:

        nr_iterations += 1

        print "iteration nr {0} ".format(nr_iterations),

        max_dx = 0.0
        norm_dx = 0.0
        residual_sum = 0.0
        for i in range(nr_stations):
            timer1 -= time.time()

            # predict visibilities for current solution

            hessian  = np.zeros((nr_timeslots, nr_parameters0, nr_parameters0), dtype = np.float64)
            gradient = np.zeros((nr_timeslots, nr_parameters0), dtype = np.float64)
            residual = np.zeros((1, ), dtype = np.float64)

            aterm_ampl = np.repeat(np.tensordot(parameters[i,:nr_parameters_ampl], Bampl, axes = ((0,), (0,)))[np.newaxis,:], nr_timeslots, axis=0)
            aterm_phase = np.exp(1j * np.tensordot(parameters[i,nr_parameters_ampl:].reshape((nr_timeslots, nr_parameters_phase)), Bphase, axes = ((1,), (0,))))

            aterm_derivatives_ampl = aterm_phase[:, np.newaxis,:,:,:]*Bampl[np.newaxis,:,:,:,:]

            aterm_derivatives_phase = 1j*aterm_ampl[:, np.newaxis,:,:,:] * aterm_phase[:, np.newaxis,:,:,:] * Bphase[np.newaxis,:,:,:,:]

            aterm_derivatives = np.concatenate((aterm_derivatives_ampl, aterm_derivatives_phase), axis=1)
            aterm_derivatives = np.ascontiguousarray(aterm_derivatives, dtype=np.complex64)

            timer0 -= time.time()
            proxy.calibrate_update(i, aterms, aterm_derivatives, hessian, gradient, residual)

            timer0 += time.time()

            residual0 = residual[0]

            residual_sum += residual[0]

            gradient = np.concatenate((np.sum(gradient[:,:nr_parameters_ampl], axis=0), gradient[:,nr_parameters_ampl:].flatten()))

            H00 = hessian[:, :nr_parameters_ampl, :nr_parameters_ampl].sum(axis=0)
            H01 = np.concatenate([hessian[t, :nr_parameters_ampl, nr_parameters_ampl:] for t in range(nr_timeslots)], axis=1)
            H10 = np.concatenate([hessian[t, nr_parameters_ampl:, :nr_parameters_ampl] for t in range(nr_timeslots)], axis=0)
            H11 = scipy.linalg.block_diag(*[hessian[t, nr_parameters_ampl:, nr_parameters_ampl:] for t in range(nr_timeslots)])

            hessian = np.block([[H00, H01],[H10, H11]])
            hessian0 = hessian

            dx = np.dot(np.linalg.pinv(hessian, pinv_tol), gradient)

            norm_dx += np.linalg.norm(dx)**2

            if max_dx <  np.amax(abs(dx)) :
                max_dx = np.amax(abs(dx))
                i_max = i

            p0 = parameters[i].copy()

            parameters[i] += solver_update_gain*dx

            aterm_ampl = np.repeat(np.tensordot(parameters[i,:nr_parameters_ampl], Bampl, axes = ((0,), (0,)))[np.newaxis,:], nr_timeslots, axis=0)
            aterm_phase = np.exp(1j * np.tensordot(parameters[i,nr_parameters_ampl:].reshape((nr_timeslots, nr_parameters_phase)), Bphase, axes = ((1,), (0,))))

            aterms0 = aterms.copy()
            aterms[:, i] = aterm_ampl * aterm_phase

            timer1 += time.time()

        dresidual = previous_residual - residual_sum
        fractional_dresidual = dresidual / residual_sum

        print max_dx, fractional_dresidual


        previous_residual = residual_sum

        converged = (nr_iterations>1) and (fractional_dresidual < 1e-6)

        if converged:
            msg = "Converged after {nr_iterations} iterations - {max_dx}".format(nr_iterations=nr_iterations, max_dx=max_dx)
            print msg
            break

        if nr_iterations == 100:
            msg = "Did not converge after {nr_iterations} iterations - {max_dx}".format(nr_iterations=nr_iterations, max_dx=max_dx)
            print msg
            #figtitle.set_text(msg)
            break

    parameters_polynomial = parameters.copy()

    for i in range(nr_stations):
        parameters_polynomial[i,:nr_parameters_ampl] = np.dot(Tampl, parameters_polynomial[i,:nr_parameters_ampl])
        for j in range(nr_timeslots):
            parameters_polynomial[i,nr_parameters_ampl+j*nr_parameters_phase:nr_parameters_ampl+(j+1)*nr_parameters_phase] = \
                np.dot(Tphase, parameters_polynomial[i,nr_parameters_ampl+j*nr_parameters_phase:nr_parameters_ampl+(j+1)*nr_parameters_phase])

    output_dataset.resize(output_dataset.shape[0]+1, axis=0)
    output_dataset[-1, :, :] = parameters_polynomial
    output_file.flush()


    timer += time.time()

    print("in {0} seconds".format(timer))
    print("in {0} seconds per iteration".format(timer/nr_iterations))
    print("spend {0} seconds in kernel".format(timer0))

    print("{0}h for entire MS".format(timer*len(pyrap.tables.taql("SELECT UNIQUE TIME FROM $table"))/nr_timesteps/3600))

output_file.close()
