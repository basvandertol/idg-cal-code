#!/usr/bin/env python


import numpy as np
import h5py
import scipy.constants as sc
import pyrap.tables
import signal
import argparse
import time
import idg

import matplotlib as mpl
mpl.use('Agg')
import idg.util as util

import astropy.io.fits as fits
import scipy.linalg
from idgwindow import idgwindow
from utils import *


ms = "/var/scratch/bvdtol/idg-demo/data/simulation.ms"

imagename = "/var/scratch/bvdtol/P23Hetdex20/image_full_ampphase_di_m.NS.int.model.fits"

print
print ms, imagename

h = fits.getheader(imagename)

padding = 1.5

N00 = h["NAXIS1"]
N0 = 9600
N = next_composite(int(N0 * padding))

cell_size = abs(h["CDELT1"]) / 180 * np.pi
#cell_size = 1.2 / 3600 / 180 * np.pi

image_size = N * cell_size
print N0, N, image_size

datacolumn = "DATA"

######################################################################
# Open measurementset
######################################################################
table0 = pyrap.tables.table(ms, readonly=False)
table = pyrap.tables.taql("SELECT * FROM $table0 WHERE ANTENNA1 != ANTENNA2")

# Read parameters from measurementset
t_ant = pyrap.tables.table(table.getkeyword("ANTENNA"))
t_spw = pyrap.tables.table(table.getkeyword("SPECTRAL_WINDOW"))
frequencies = np.asarray(t_spw[0]['CHAN_FREQ'], dtype=np.float32)


######################################################################
# Parameters
######################################################################
nr_stations      = len(t_ant)
nr_baselines     = (nr_stations * (nr_stations - 1)) / 2
nr_channels      = table[0][datacolumn].shape[0]

nr_timeslots     = 40
nr_timesteps_per_slot = 4

nr_timesteps     =  nr_timeslots * nr_timesteps_per_slot

nr_correlations  = 4
grid_size        = N
subgrid_size     = 32

taper_support    = 7
wterm_support    = 5
aterm_support    = 5

kernel_size      = taper_support + wterm_support + aterm_support

nr_parameters_ampl = 6
nr_parameters_phase = 3
nr_parameters0 = nr_parameters_ampl + nr_parameters_phase
nr_parameters = nr_parameters_ampl + nr_parameters_phase*nr_timeslots

solver_update_gain = 0.5
pinv_tol = 1e-9

#########################################

input_filename = "fitted-parameters-{0}ampl-{1}phase.hdf5".format(nr_parameters_ampl, nr_parameters_phase)

with h5py.File(input_filename, "r") as input_file:

    dataset = input_file["parameters"]

    parameters_in = dataset[:]

# Initialize empty buffers
rowid        = np.zeros(shape=(nr_baselines, nr_timesteps),
                        dtype=np.int)
uvw          = np.zeros(shape=(nr_baselines, nr_timesteps),
                        dtype=idg.uvwtype)
visibilities = np.zeros(shape=(nr_baselines, nr_timesteps, nr_channels,
                            nr_correlations),
                        dtype=idg.visibilitiestype)
weights      = np.zeros(shape=(nr_baselines, nr_timesteps, nr_channels,
                            nr_correlations),
                        dtype=np.float32)
baselines    = np.zeros(shape=(nr_baselines),
                        dtype=idg.baselinetype)
img          = np.zeros(shape=(nr_correlations, grid_size, grid_size),
                        dtype=idg.gridtype)


#proxy = idg.CPU.Optimized(nr_correlations, subgrid_size)

proxy_ref = idg.CPU.Optimized(nr_correlations, subgrid_size)


if "proxy" not in globals():
    proxy = idg.HybridCUDA.GenericOptimized(nr_correlations, subgrid_size)

if "taper" not in globals():
    # Initialize taper
    taper = idgwindow(subgrid_size, taper_support, padding)
    taper2 = np.outer(taper, taper).astype(np.float32)

    taper_ = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(taper)))
    taper_grid = np.zeros(grid_size, dtype=np.complex128)
    taper_grid[(grid_size-subgrid_size)/2:(grid_size+subgrid_size)/2] = taper_ * np.exp(-1j*np.linspace(-np.pi/2, np.pi/2, subgrid_size, endpoint=False))
    taper_grid = np.fft.fftshift(np.fft.ifft(np.fft.ifftshift(taper_grid))).real*grid_size/subgrid_size

    d = fits.getdata(imagename)
    d = d[:,:, N00/2-N0/2:N00/2+N0/2,N00/2-N0/2:N00/2+N0/2]

    print "{0} non-zero pixels.".format(np.sum(d != 0.0))

    taper_grid0 = taper_grid[(N-N0)/2:(N+N0)/2]

    img[0, (N-N0)/2:(N+N0)/2, (N-N0)/2:(N+N0)/2] = d[0,0,:,:]/np.outer(taper_grid0, taper_grid0)
    img[3, (N-N0)/2:(N+N0)/2, (N-N0)/2:(N+N0)/2] = d[0,0,:,:]/np.outer(taper_grid0, taper_grid0)

    #grid = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(img, axes = (1,2))), axes=(1,2)).astype(np.complex64)

    grid = img.copy()
    proxy.transform(idg.ImageDomainToFourierDomain, grid)

p = []

nr_timesteps_in_ms = len(pyrap.tables.taql("SELECT UNIQUE TIME FROM $table"))


for interval_idx, interval_start in enumerate(range(0,nr_timesteps_in_ms-nr_timesteps+1, nr_timesteps)):

    start_row = nr_baselines * interval_start
    nr_rows = nr_baselines * nr_timesteps

    start_row = nr_baselines * interval_start
    nr_rows = nr_baselines * nr_timesteps

    # Read nr_timesteps samples for all baselines including auto correlations
    timestamp_block = table.getcol('TIME',
                                    startrow = start_row,
                                    nrow = nr_rows)
    antenna1_block  = table.getcol('ANTENNA1',
                                    startrow = start_row,
                                    nrow = nr_rows)
    antenna2_block  = table.getcol('ANTENNA2',
                                    startrow = start_row,
                                    nrow = nr_rows)
    uvw_block       = table.getcol('UVW',
                                    startrow = start_row,
                                    nrow = nr_rows)
    vis_block       = table.getcol(datacolumn,
                                    startrow = start_row,
                                    nrow = nr_rows)
    weight_block    = table.getcol("WEIGHT_SPECTRUM",
                                    startrow = start_row,
                                    nrow = nr_rows)
    flags_block     = table.getcol('FLAG',
                                    startrow = start_row,
                                    nrow = nr_rows)

    # In simulation all data is good
    flags_block[:] = False

    rowid_block     = np.arange(nr_rows)

    uvw_block[:,1:3] = -uvw_block[:,1:3]

    print start_row, nr_rows

    # Change precision
    uvw_block = uvw_block.astype(np.float32)

    # Reshape data
    antenna1_block = np.reshape(antenna1_block,
                                newshape=(nr_timesteps, nr_baselines))
    antenna2_block = np.reshape(antenna2_block,
                                newshape=(nr_timesteps, nr_baselines))
    uvw_block = np.reshape(uvw_block,
                            newshape=(nr_timesteps, nr_baselines, 3))
    vis_block = np.reshape(vis_block,
                            newshape=(nr_timesteps, nr_baselines,
                                        nr_channels, nr_correlations))
    weight_block = np.reshape(weight_block,
                            newshape=(nr_timesteps, nr_baselines,
                                        nr_channels, nr_correlations))
    rowid_block = np.reshape(rowid_block,
                                newshape=(nr_timesteps, nr_baselines))

    # Transpose data
    for t in range(nr_timesteps):
        for bl in range(nr_baselines):
            # Set baselines
            antenna1 = antenna1_block[t][bl]
            antenna2 = antenna2_block[t][bl]

            baselines[bl] = (antenna1, antenna2)

            # Set uvw
            uvw_ = uvw_block[t][bl]
            uvw[bl][t] = uvw_

            # Set visibilities
            visibilities[bl][t] = vis_block[t][bl]

            weights[bl][t] = weight_block[t][bl] #* (antenna1 != 51) * (antenna2 != 51)

            rowid[bl][t] = rowid_block[t][bl]

    # Grid visibilities
    w_step = 400.0

    shift = np.array((0.0, 0.0, 0.0), dtype=np.float32)

    aterms         = util.get_identity_aterms(
                        nr_timeslots, nr_stations, subgrid_size, nr_correlations)
    aterms_offsets = util.get_example_aterms_offset(
                        nr_timeslots, nr_timesteps)

    B0 = np.ones((1, subgrid_size, subgrid_size,1))

    s = image_size/subgrid_size*(subgrid_size-1)
    l = s * np.linspace(-0.5, 0.5, subgrid_size)
    m = -s * np.linspace(-0.5, 0.5, subgrid_size)

    B1,B2 = np.meshgrid(l,m)


    B1 = B1[np.newaxis, :, :, np.newaxis]
    B2 = B2[np.newaxis, :, :, np.newaxis]
    B3 = B1*B1
    B4 = B2*B2
    B5 = B1*B2
    B6 = B1*B1*B1
    B7 = B1*B1*B2
    B8 = B1*B2*B2
    B9 = B2*B2*B2
    B10 = B1*B1*B1*B1
    B11= B1*B1*B1*B2
    B12 = B1*B1*B2*B2
    B13 = B1*B2*B2*B2
    B14 = B2*B2*B2*B2
    B15 = B1*B1*B1*B1*B1
    B16 = B1*B1*B1*B1*B2
    B17 = B1*B1*B1*B2*B2
    B18 = B1*B1*B2*B2*B2
    B19 = B1*B2*B2*B2*B2
    B20 = B2*B2*B2*B2*B2

    base_polynomial = np.concatenate((B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,B16,B17,B18,B19,B20))

    Bampl = np.kron(base_polynomial[:nr_parameters_ampl], np.array([1.0, 0.0, 0.0, 1.0]))
    Bphase = np.kron(base_polynomial[:nr_parameters_phase], np.array([1.0, 0.0, 0.0, 1.0]))

    X1 =  np.ones((nr_stations, 1))
    X0 =  np.zeros((nr_stations, 1))


    parameters = np.concatenate((X1,) + (nr_parameters-1)*(X0,), axis=1)

    parameters = parameters_in[interval_idx, :, :]

    ###############################################
    # Simulate
    ###############################################
    parameters0 = parameters.copy()

    aterm_ampl = np.tensordot(parameters0[:,:nr_parameters_ampl] , Bampl, axes = ((1,), (0,)))
    aterm_phase = np.exp(1j*np.tensordot(parameters0[:,nr_parameters_ampl:].reshape((nr_stations, nr_timeslots, nr_parameters_phase)), Bphase, axes = ((2,), (0,))))
    aterms[:,:,:,:,:] = aterm_phase.transpose((1,0,2,3,4))*aterm_ampl

    proxy_ref.degridding(
        w_step,
        shift,
        cell_size, kernel_size,
        subgrid_size,
        frequencies, visibilities,
        uvw, baselines, grid, aterms, aterms_offsets, taper2)

    sigma = np.sqrt(0.5/weights)
    visibilities += sigma*np.random.standard_normal(visibilities.shape)
    visibilities += sigma*1j*np.random.standard_normal(visibilities.shape)
    table.putcol(datacolumn,visibilities.transpose((1,0,2,3)).reshape(nr_timesteps * nr_baselines, nr_channels, nr_correlations), startrow = start_row, nrow = nr_rows)
