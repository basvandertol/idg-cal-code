import numpy as np
import astropy.io.fits as fits
import os.path
import casacore.tables
from utils import *
import h5py
import os.path

imagedir = "/var/scratch/bvdtol/idg-demo"

imagename = "/var/scratch/bvdtol/idg-demo/case-4/wsclean-image.fits"

atermname = "aterm-idg-cal-20min-32s.fits"

datadir = "/var/scratch/bvdtol/P23Hetdex20"

input_filename = "simulation1-6ampl-3phase.hdf5"

station_blacklist = []

with h5py.File(input_filename, "r") as input_file:

    dataset = input_file["parameters"]

    p = dataset[:]

    nr_timeslots          = dataset.attrs["nr_timeslots"]
    nr_timesteps_per_slot = dataset.attrs["nr_timesteps_per_slot"]
    nr_parameters_ampl    = dataset.attrs["nr_parameters_ampl"]
    nr_parameters_phase   = dataset.attrs["nr_parameters_phase"]
    time_start            = dataset.attrs["time_start"]
    time_step             = dataset.attrs["time_step"]
    subgrid_size          = dataset.attrs["subgrid_size"]
    image_size            = dataset.attrs["image_size"]


with h5py.File("fitted-parameters-6ampl-3phase.hdf5", "r") as input_file:

    dataset = input_file["parameters"]

    p = dataset[:22,:,:]

nr_stations = p.shape[1]

subgrid_size = 64
aterm_support = 5

B0 = np.ones((1, subgrid_size, subgrid_size,1))

x = np.linspace(-0.5, 0.5, subgrid_size)

B1,B2 = np.meshgrid(x,x)

B1 = B1[np.newaxis, :, :, np.newaxis]
B2 = B2[np.newaxis, :, :, np.newaxis]
B3 = B1*B1
B4 = B2*B2
B5 = B1*B2
B6 = B1*B1*B1
B7 = B1*B1*B2
B8 = B1*B2*B2
B9 = B2*B2*B2
B10 = B1*B1*B1*B1
B11= B1*B1*B1*B2
B12 = B1*B1*B2*B2
B13 = B1*B2*B2*B2
B14 = B2*B2*B2*B2
B15 = B1*B1*B1*B1*B1
B16 = B1*B1*B1*B1*B2
B17 = B1*B1*B1*B2*B2
B18 = B1*B1*B2*B2*B2
B19 = B1*B2*B2*B2*B2
B20 = B2*B2*B2*B2*B2


BB = np.concatenate((B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,B16,B17,B18,B19,B20))

B = np.kron(BB, np.array([1.0, 0.0, 0.0, 1.0]))

Bampl = B[:nr_parameters_ampl]
Bphase = B[:nr_parameters_phase]

N_freq = 1

N_time = p.shape[0] * nr_timeslots
dt = time_step * nr_timesteps_per_slot
t0 = time_start + time_step/2

header=fits.getheader(os.path.join(imagedir, imagename))

# AXIS 3 is FREQUENCY in image
# becomes AXIS 5 in diagonal aterm fits file
for key in header.keys():
    if key[-1] == "3":
        header[key[:-1]+"5"]=header[key]

header['NAXIS']=6

#Axis 1: l
header['NAXIS1']=subgrid_size
header['CRPIX1']=(subgrid_size+1)/2
header['CDELT1']=-image_size/subgrid_size/np.pi*180

#Axis 2: m
header['NAXIS2']=subgrid_size
header['CRPIX2']=(subgrid_size+1)/2
header['CDELT2']=image_size/subgrid_size/np.pi*180

#Axis 3: MATRIX
header['CTYPE3']='MATRIX'
header['NAXIS3']=4
header['CRVAL3']=1
header['CRPIX3']=1
header['CDELT3']=1

#Axis 4: ANTENNA
header['CTYPE4'] = 'ANTENNA'
header['NAXIS4'] = nr_stations
header['CRVAL4']=1
header['CRPIX4']=1
header['CDELT4']=1

#Axis 5: FREQUENCY

#Axis 6: TIME
header['CTYPE6']='TIME'
header['NAXIS6']=N_time
header['CRPIX6']=1
header['CDELT6']=dt
header['CRVAL6']=t0

Aterm=np.zeros((N_time, N_freq, nr_stations, 4, subgrid_size, subgrid_size))

for i in range(N_time):
    print i

    j = i / nr_timeslots
    k = i % nr_timeslots

    for station in range(nr_stations):

        if station not in station_blacklist:

            ampl = np.tensordot(p[j,station,:nr_parameters_ampl], Bampl, axes = ((0,), (0,)))
            phase = np.tensordot(p[j,station,nr_parameters_ampl+k*nr_parameters_phase:nr_parameters_ampl+(k+1)*nr_parameters_phase], Bphase, axes = ((0,), (0,)))

            #A = mask_aterm(ampl * np.exp(1j * phase), aterm_support)
            A = ampl * np.exp(1j * phase)

            Aterm[i, 0, station, 0,:,:] = A[::-1,::-1,0].real
            Aterm[i, 0, station, 1,:,:] = A[::-1,::-1,0].imag
            Aterm[i, 0, station, 2,:,:] = A[::-1,::-1,3].real
            Aterm[i, 0, station, 3,:,:] = A[::-1,::-1,3].imag

fits.writeto(os.path.join(datadir, atermname), data=Aterm, header=header, overwrite=True)
