#!/usr/bin/env python

import numpy as np
import astropy.io.fits as fits
import os.path
import casacore.tables
from utils import *
import h5py
import os.path

solfile1 = "DDS3_full_4910677203.026532_smoothed.npz"
solfile2 = "DDS3_full_slow_4910677203.026532_merged.npz"

datadir = "/var/scratch/bvdtol/P23Hetdex20"

imagename = "/var/scratch/bvdtol/idg-demo/case-4/wsclean-image.fits"


ms = "/var/scratch/bvdtol/P23Hetdex20/L232875_SB120_uv.dppp.pre-cal_124B2FCD4t_144MHz.pre-cal.ms.archive"
t = casacore.tables.table(ms)
ft = casacore.tables.table(t.getkeyword('FIELD'))
ra,dec = ft[0]['PHASE_DIR'][0]

nr_timeslots     = 40
nr_timesteps_per_slot = 4

nr_parameters_ampl = 6
nr_parameters_phase = 3
nr_parameters = nr_parameters_phase*nr_timeslots + nr_parameters_ampl

nr_stations = 62

solutions1 = np.load(os.path.join(datadir, solfile1))
solutions2 = np.load(os.path.join(datadir, solfile2))


facets1 = solutions1['SkyModel']
facets2 = solutions1['SkyModel']

time_idx = 0

if "Sols1" not in globals(): Sols1 = solutions1["Sols"]
if "Sols2" not in globals(): Sols2 = solutions2["Sols"]


h = fits.getheader(imagename)
padding = 1.5
N0 = h["NAXIS1"]
N = next_composite(int(N0 * padding))
cell_size = abs(h["CDELT1"]) / 180 * np.pi
image_size = N * cell_size

subgrid_size = 32
aterm_support = 5

l = facets1['l']
m = facets1['m']


y = l[:, np.newaxis]*1.0
x = m[:, np.newaxis]*1.0

b = []
for order in range(10):
    for order_x in range(order+1):
        order_y = order - order_x
        b.append(x**order_x * y**order_y)

b = np.concatenate(b, axis=1)

N_time = len(Sols1)
N_ant = len(solutions1["StationNames"])


sb = 12

Sols2_iter = iter(Sols2)
s2 = Sols2_iter.next()

pp =  []
aa = []
for i, s1 in enumerate(Sols1):
    print i

    # Advance the long term solutions if the start time of the short solutions is later than the end time of the long solutions
    if s1[0] >= s2[1]: s2 = Sols2_iter.next()

    s = s1[2][sb,:,:,0,0] * s2[2][sb,:,:,0,0]
    a = np.abs(s)
    p = np.angle(s)

    aa.append(a)
    pp.append(p)

aa = np.array(aa)
pp = np.array(pp)
pp = np.unwrap(pp, axis=0)

aa = np.mean(aa.reshape((-1,40,62,45)), axis=1)
b1 = b[:,:nr_parameters_ampl]
T = np.dot(np.linalg.inv(np.dot(b1.T, b1)),b1.T)
p_ampl = np.tensordot(aa, T, axes = (2,1))
# force first term (zero order / constant) to one
p_ampl[:,:,0] = 1.0


b1 = b[:,:nr_parameters_phase]
T = np.dot(np.linalg.inv(np.dot(b1.T, b1)),b1.T)
p_phase = np.tensordot(pp, T, axes = (2,1))
print p_phase.shape
# force first term (zero order / constant) to zero
p_phase[:,:,0] = 0.0
p_phase = p_phase.reshape((-1,40,62,3)).transpose((0,2,1,3)).reshape((24,62,-1))

parameters = np.concatenate((p_ampl, p_phase), axis=2)

output_filename = "fitted-parameters-{0}ampl-{1}phase.hdf5".format(nr_parameters_ampl, nr_parameters_phase)
try:
    output_file = h5py.File(output_filename, "w")
except:
    pass
try:
    output_dataset = output_file.create_dataset("parameters", shape=(0,nr_stations, nr_parameters), maxshape=(None, nr_stations, nr_parameters), dtype=np.float32, chunks = True)
except:
    pass


output_dataset.attrs["nr_timeslots"] = nr_timeslots
output_dataset.attrs["nr_timesteps_per_slot"] = nr_timesteps_per_slot
output_dataset.attrs["nr_parameters_ampl"] = nr_parameters_ampl
output_dataset.attrs["nr_parameters_phase"] = nr_parameters_phase
output_dataset.attrs["subgrid_size"] = subgrid_size
output_dataset.attrs["image_size"] = image_size
output_dataset.attrs["imagename"] = imagename
output_dataset.attrs["ra"] = ra
output_dataset.attrs["dec"] = dec

output_dataset.attrs["time_start"] = t[0]['TIME']
output_dataset.attrs["time_step"] = t[0]['INTERVAL']


output_dataset.resize(parameters.shape)
output_dataset[:] = parameters

output_file.flush()

