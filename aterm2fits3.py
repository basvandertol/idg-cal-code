import numpy as np
import astropy.io.fits as fits
import os.path
import casacore.tables
from utils import *
import h5py
import os.path

solfile1 = "DDS3_full_4910677203.026532_smoothed.npz"
solfile2 = "DDS3_full_slow_4910677203.026532_merged.npz"

imagedir = "/var/scratch/bvdtol/idg-demo"

imagename = "/var/scratch/bvdtol/idg-demo/wsclean-image.fits"

atermname = "aterm-idg-cal-20min-32s.fits"

datadir = "/var/scratch/bvdtol/P23Hetdex20"

#input_filename = "parameters2.hdf5"
input_filename = "parameters-6-10.hdf5"

with h5py.File(input_filename, "r") as input_file:

    dataset = input_file["parameters"]

    p = dataset[:]

    nr_timeslots          = dataset.attrs["nr_timeslots"]
    nr_timesteps_per_slot = dataset.attrs["nr_timesteps_per_slot"]
    nr_parameters_ampl    = dataset.attrs["nr_parameters_ampl"]
    nr_parameters_phase   = dataset.attrs["nr_parameters_phase"]
    time_start            = dataset.attrs["time_start"]
    time_step             = dataset.attrs["time_step"]
    subgrid_size          = dataset.attrs["subgrid_size"]
    image_size            = dataset.attrs["image_size"]

nr_timesteps     = nr_timeslots * nr_timesteps_per_slot
scaling          = (image_size * (subgrid_size - 1)) / subgrid_size

nr_stations = p.shape[1]

p1 = p[:,:,nr_parameters_ampl:].reshape(-1,nr_stations,nr_timeslots,nr_parameters_phase).transpose((0,2,1,3)).reshape(-1,nr_stations,nr_parameters_phase)


p2 = p[:,:,:nr_parameters_ampl]

station_blacklist = []

solutions1 = np.load(os.path.join(datadir, solfile1))
solutions2 = np.load(os.path.join(datadir, solfile2))


facets1 = solutions1['SkyModel']
facets2 = solutions1['SkyModel']

time_idx = 0

if "Sols1" not in globals(): Sols1 = solutions1["Sols"]
if "Sols2" not in globals(): Sols2 = solutions2["Sols"]


h = fits.getheader(imagename)
padding = 1.2
N0 = h["NAXIS1"]
N = next_composite(int(N0 * padding))
cell_size = abs(h["CDELT1"]) / 180 * np.pi
image_size = N * cell_size

subgrid_size = 64
aterm_support = 5


l = facets1['l']
m = facets1['m']


y = l[:, np.newaxis]/scaling
x = m[:, np.newaxis]/scaling

b = []
for order in range(6):
    for order_x in range(order+1):
        order_y = order - order_x
        b.append(x**order_x * y**order)

b = np.concatenate(b, axis=1)

phase = np.tensordot(p1, b[:,:nr_parameters_phase], axes=((-1),(-1)))
ampl = np.tensordot(p2, b[:,:nr_parameters_ampl], axes=((-1),(-1)))

dt = time_step * nr_timesteps_per_slot
time_range1 = np.arange(phase.shape[0])*dt+dt/2 + time_start
dt1 = dt*nr_timeslots
time_range11 = np.arange(ampl.shape[0])*dt1+dt1/2 + time_start


N_time = len(Sols1)
N_ant = len(solutions1["StationNames"])

dt = Sols1[2][0] - Sols1[1][0]
t0 = (Sols1[1][0] + Sols1[1][1])/2 - dt

sb = 12

Sols2_iter = iter(Sols2)
s2 = Sols2_iter.next()

pp =  []
aa = []
for i, s1 in enumerate(Sols1):
    print i

    # Advance the long term solutions if the start time of the short solutions is later than the end time of the long solutions
    if s1[0] >= s2[1]: s2 = Sols2_iter.next()

    s = s1[2][sb,:,:,0,0] * s2[2][sb,:,:,0,0]
    a = np.abs(s)
    p = np.angle(s)

    aa.append(a)
    pp.append(p)

aa = np.array(aa)
pp = np.array(pp)
pp = np.unwrap(pp, axis=0)

time_range2 = np.arange(pp.shape[0])*dt + t0



# brightest source, descending : 11,  0, 13,  5, 38, 28

source = 11
station = 60

for source in np.argsort(facets1['SumI'])[::-1][:3]:
    figure()
    plot(time_range2, pp[:,[station],source] - pp[:,[0],source], '.-')
    plot(time_range2, aa[:,[station],source], '.-')
    #xlim(t0,t0+360*dt)
    ylim(-3,3)
    plot(time_range1, phase[:,[station],source] - phase[:,[0],source], '.-')
    plot(time_range11, ampl[:,[station],source], '.-')
    #xlim(t0,t0+360*dt)
    ylim(-3,3)


raise RuntimeError("stop")


image_size = 0.25

N_freq = 1

N_time = phase.shape[0]

header=fits.getheader(os.path.join(imagedir, imagename))

# AXIS 3 is FREQUENCY in image
# becomes AXIS 5 in diagonal aterm fits file
for key in header.keys():
    if key[-1] == "3":
        header[key[:-1]+"5"]=header[key]

header['NAXIS']=6

#Axis 1: l
header['NAXIS1']=subgrid_size
header['CRPIX1']=(subgrid_size+1)/2
header['CDELT1']=-image_size/subgrid_size/np.pi*180

#Axis 2: m
header['NAXIS2']=subgrid_size
header['CRPIX2']=(subgrid_size+1)/2
header['CDELT2']=image_size/subgrid_size/np.pi*180

#Axis 3: MATRIX
header['CTYPE3']='MATRIX'
header['NAXIS3']=4
header['CRVAL3']=1
header['CRPIX3']=1
header['CDELT3']=1

#Axis 4: ANTENNA
header['CTYPE4'] = 'ANTENNA'
header['NAXIS4'] = nr_stations
header['CRVAL4']=1
header['CRPIX4']=1
header['CDELT4']=1

#Axis 5: FREQUENCY

#Axis 6: TIME
header['CTYPE6']='TIME'
header['NAXIS6']=N_time
header['CRPIX6']=1
header['CDELT6']= time_step * nr_timesteps_per_slot
header['CRVAL6']= time_range1[0]

Aterm=np.zeros((N_time, N_freq, nr_stations, 4, subgrid_size, subgrid_size))



###########################################
N = aterm_support
u_range = np.linspace(-(N-1)/2,(N-1)/2,N)
v_range = np.linspace(-(N-1)/2,(N-1)/2,N)

u,v = np.meshgrid(u_range, v_range)

u = u.flatten()
v = v.flatten()

A = []
for l,m in facets1[['l', 'm']]:
    A.append(np.exp(-2*np.pi*1j * (-l*u + m*v)/image_size))

A = np.array(A)
Ainv = np.linalg.pinv(A, rcond = 1e-2)
#########################################


for i in range(N_time):
    print i

    j = i / nr_timeslots
    k = i % nr_timeslots

    for station in range(nr_stations):

        if station not in station_blacklist:

            phase1 = phase[j, station, :, np.newaxis]
            ampl1 = ampl[k, station, :, np.newaxis]

            s = ampl1 * np.exp(1j * phase1)

            c = np.dot(Ainv, s)

            N1 = subgrid_size
            N2 = aterm_support
            c1 = np.zeros((N1,N1), dtype=np.complex)
            c1[N1/2-N2/2:N1/2+N2/2+1, N1/2-N2/2:N1/2+N2/2+1] = c.reshape((N,N))[N/2-N2/2:N/2+N2/2+1, N/2-N2/2:N/2+N2/2+1]
            C = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(c1)))
            Aterm[i, 0, station, 0,:,:] = C.real
            Aterm[i, 0, station, 1,:,:] = C.imag
            Aterm[i, 0, station, 2,:,:] = C.real
            Aterm[i, 0, station, 3,:,:] = C.imag


fits.writeto(os.path.join(datadir, atermname), data=Aterm, header=header, overwrite=True)
